;;; publish.el --- Publish reveal.js presentation, HTML, and PDF from Org sources
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2017-2019 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; License: GPLv3

;;; Commentary:
;; Mostly, settings from emacs-reveal-publish are used.
;; Besides, non-free logos are published here.

;;; Code:
(add-to-list 'load-path
	     (expand-file-name
	      "../emacs-reveal/" (file-name-directory load-file-name)))
(require 'emacs-reveal-publish)

;; For self-contained presentations whose GitLab Pages artifacts can
;; be used offline, the file index.css needs to be included.  That
;; file is available at oer.gitlab.io.  To embed here, different
;; alternatives could be used.  Embedding in every HTML head causes
;; larger HTML files with redundant contents.  Thus, download once and
;; publish.  Link from Org file if necessary.
(require 'url)
(unless (file-exists-p "index.css") ; Avoid download in case of local copy.
  (url-copy-file "https://oer.gitlab.io/index.css" "index.css"))

;; For figure positions that are similar to HTML layout, position
;; figures "here" in LaTeX export with float package.  See there:
;; https://ctan.org/pkg/float
(setq emacs-reveal-latex-figure-float "H")

;; Rely on setup of emacs-reveal.  Add export of non-free parts.
(add-to-list 'org-publish-project-alist
	     (list "title-logos"
		   :base-directory "non-free-logos/title-slide"
		   :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
		   :publishing-directory "./public/title-slide"
		   :publishing-function 'org-publish-attachment))
(add-to-list 'org-publish-project-alist
	     (list "index-css"
		   :base-directory "."
		   :include '("index.css")
		   :exclude ".*"
		   :publishing-function '(org-publish-attachment)
		   :publishing-directory "./public"))

(provide 'publish)
;;; publish.el ends here
